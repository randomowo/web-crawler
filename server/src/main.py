import json
import re
import sys
import time
from dataclasses import asdict
from dataclasses import dataclass
from dataclasses import field

import requests
from bs4 import BeautifulSoup
from flask import Flask
from flask import request

app = Flask(__name__)

HEADERS = {
    'Content-Type': 'application/json',
}

regex_type_to_regex = {
    'url': re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        # domain...
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE),
    'href': re.compile(r'^(?:http|ftp)s?://'),
}


DEPTH = 2

CACHE = dict()

if len(sys.argv) > 1 and sys.argv[1] in ['-d', '--debug']:
    DEBUG = True
else:
    DEBUG = False


@dataclass
class Res:
    url: str = ''
    desc: str = ''
    title: str = ''
    found: bool = False
    count: int = 0
    res_list: list['Res'] = field(default_factory=list)

    def to_json(self):
        res = {
            'url': self.url,
            'desc': self.desc,
            'title': self.title,
            'found': self.found,
            'count': self.count,
            'links': list(),
        }
        for link in self.res_list:
            res['links'].append(link.to_json())

        return res


@dataclass
class CachedRes(Res):
    ts: float = .0
    links: set[str] = field(default_factory=set)
    tags: set[str] = field(default_factory=set)
    text: str = ''

    def update(self, url, desc, title, found, links, tag, text):
        self.url = url
        self.desc = desc
        self.title = title
        self.found = found
        self.count = len(links)
        self.links = links
        self.ts = time.time()
        if tag is not None:
            self.tags.add(tag)
        self.text = text


@app.route('/', methods=['GET'])
def index():
    url = request.args.get('url', default='')
    max_depth = request.args.get('depth', default=DEPTH)
    search = request.args.get('search', default='').lower()
    try:
        max_depth = int(max_depth)
    except:
        return 'The passed max_depth is not int', 400

    if max_depth < 1:
        return 'The passed depth lower than 1', 400

    if not validate_url(url):
        return 'The passed url is not valid', 400

    if not search:
        return 'search param not passed', 400

    t = time.time()
    result, count, c_count = spider(
        url,
        re.compile(search, re.IGNORECASE),
        max_depth
    )
    if DEBUG:
        print(
            f'\nSEARCH FOR {count} LINKS'
            f'(CACHE {c_count}) ELLAPSED: {time.time() - t}'
        )

    t = time.time()

    if result:
        result = result.to_json()
    else:
        return f'Can not get page by {url}', 400

    if DEBUG:
        print(f'SERIALIZATION ELLAPSED: {time.time() - t}\n')

    return json.dumps(result, indent=1), 200, HEADERS


def validate_url(url):
    return re.match(regex_type_to_regex['url'], url) is not None


def spider(url, search, max_depth):
    if DEBUG:
        print(f'CACHE SIZE {len(CACHE)}\n')

    return _spider(url, search, max_depth)


def cache_flush(url):
    cached = CACHE.get(url)
    if cached and time.time() - cached.ts > 60 * 60:
        del CACHE[url]
    return CACHE.get(url)


def can_use_cache(url, search, is_max_depth):
    cached = cache_flush(url)
    return (
        cached and
        (len(cached.links) or is_max_depth)
    )


def get_res(url, search):
    found = False
    tag = None
    res = Res()

    rt = time.time()
    try:
        page = requests.get(url, timeout=2)
    except:
        if DEBUG:
            print(f'{url} timeout')
        return None, None, None, None, None
    rt = time.time() - rt

    bs = BeautifulSoup(page.text, 'html.parser')

    text = bs.text
    st = time.time()
    if re.search(search, bs.text) is not None:
        found = True
        tag = search.pattern
    st = time.time() - st

    res.url = url

    title = bs.find('title')
    res.title = title.string if title else ''

    desc = bs.find('meta', attrs={'name': 'description'})
    res.desc = desc.get('content') if desc else ''

    res.found = found

    links = set(
        link.get('href')
        for link in bs.findAll('a', attrs={'href': regex_type_to_regex['href']})
    )

    return res, {'text': text, 'tag': tag}, links, rt, st


def _spider(url, search, max_depth, depth=0):
    res = None
    tag = None
    text = None
    count = 1
    c_count = 0
    search_time = 0
    request_time = 0
    if depth <= max_depth:
        if not can_use_cache(url, search, max_depth == depth):
            res, reg, links, request_time, search_time = get_res(url, search)
            if not res:
                return None, 0, 0

            tag = reg['tag']
            text = reg['text']
        else:
            found = False
            c_count = 1
            cached_res = CACHE[url]

            t = time.time()
            if search.pattern in cached_res.tags or re.search(search, cached_res.text):
                tag = search.pattern
                found = True
            search_time = time.time() - t

            res = Res(cached_res.url, cached_res.desc,
                      cached_res.title, found)
            text = cached_res.text
            links = cached_res.links

        new_res_list = []
        for link in links:
            linked_page, c, cc = _spider(link, search, max_depth, depth + 1)

            if not linked_page:
                continue

            count += c
            c_count += cc

            new_res_list.append(linked_page)

        if new_res_list:
            res.res_list = new_res_list
        res.count = len(res.res_list)

        if DEBUG:
            print(
                f'{"cached, " if c_count else ""}'
                f'{"found" if res.found else "not found"} on {url}'
            )
            if not c_count:
                print(
                    f'\t{request_time=}\n'
                    f'\t{search_time=}'
                )

        cached_res = CACHE.get(url, CachedRes())
        cached_res.update(
            res.url,
            res.desc,
            res.title,
            res.found,
            links,
            tag,
            text
        )
        CACHE[url] = cached_res

    return res, count, c_count


app.run(debug=DEBUG)
