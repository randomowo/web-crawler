#include "CrawlerClient.h"

#include <conio.h>
#include <sstream>

namespace Crawler101202 {

	CrawlerClient::CrawlerClient()
		: ServerSocketFd(NULL), IsRunning(true), Url(""), SearchStr(""), DeepCount(2)
	{}

	CrawlerClient::~CrawlerClient()
	{
		shutdown(ServerSocketFd, SD_BOTH);
		closesocket(ServerSocketFd);
	}

	void CrawlerClient::EstablishConnection()
	{
		addrinfo Hints, *ServInfo;
		bzero(&Hints, sizeof(Hints));
		Hints.ai_family   = AF_INET; 
		Hints.ai_socktype = SOCK_STREAM; //TCP
		const int rv = getaddrinfo(CRAWLER_IP_ADDR, PortNumber, &Hints, &ServInfo);
		if (rv != 0 || ServInfo == nullptr)
		{
			std::cout << "Error in connection" << std::endl;
			return;
		}
		
		ServerSocketFd = socket(ServInfo->ai_family, ServInfo->ai_socktype, ServInfo->ai_protocol);
		if (ServerSocketFd <= 0)
		{
			perror("ERROR opening socket");
			return;
		}
		if (connect(ServerSocketFd, ServInfo->ai_addr, ServInfo->ai_addrlen) == -1)
		{
			closesocket(ServerSocketFd);
			perror("ERROR connection failed");
			return;
		}
		freeaddrinfo(ServInfo); // all done with this structure
	}

	
	void CrawlerClient::SendGET()
	{
		std::string get_request;
		//Type of req
		get_request.append("GET ");
		//url
		get_request.append("/?url="); 
		get_request.append(Url);
		get_request.append("&depth=");
		get_request.append(std::to_string(DeepCount));
		get_request.append("&search=");
		get_request.append(SearchStr + " ");
		//version
		get_request.append("HTTP/1.1");
		get_request.append("\r\n");
		//server id
		get_request.append("Host:");
		get_request.append(CRAWLER_IP_ADDR);
		get_request.append(":");
		get_request.append(PortNumber);
		get_request.append("\r\n");
		//Not necessary info
		get_request.append("User-Agent:curl/7.73.0\r\n");
		get_request.append("Accept:*/*\r\n");
		get_request.append("\r\n");
		if (send(ServerSocketFd, get_request.c_str(), get_request.size(), 0) < 0 && IsRunning)
		{
			WSAGetLastError();
		}
	}

	void CrawlerClient::RecieveData()
	{
		char buffer[BUFFER_RECIEVE_SIZE];
		bzero(buffer, BUFFER_RECIEVE_SIZE);
		std::cout << "Waiting for response from Server" << std::endl;
		int numbytes = recv(ServerSocketFd, buffer, BUFFER_RECIEVE_SIZE, 0);

		// Reading server response
		if (numbytes <= 0)
		{
			perror("ERROR reading from socket");
			return;
		}
		else
		{
			HTTPAnswerHeader header;
			const int header_size = sizeof(HTTPAnswerHeader);
			memcpy(&header, &buffer[0], header_size);
			std::printf("Get Packet with size: %i, and status code: %c%c%c\n", numbytes, header.StatusCode[0], header.StatusCode[1], header.StatusCode[2]);

			if (CHECK_STATUS_OK(header.StatusCode) && numbytes <= header_size)
			{
				bzero(buffer, BUFFER_RECIEVE_SIZE);
				numbytes = recv(ServerSocketFd, buffer, BUFFER_RECIEVE_SIZE, 0);
				if (numbytes <= 0)
				{
					perror("ERROR reading disconnect");
					return;
				}
				else
				{
					HTTPAnswerPacket packet;
					size_t offset = ParseAnswer(buffer, packet);
					size_t actual_read_bytes = static_cast<size_t>(numbytes) - offset;
					std::cout << "Read bytes: " << actual_read_bytes << std::endl;
					char* data_buf = new char[packet.ContentLength];
					//write to json data buffer if present 
					if (actual_read_bytes > 0)
						memcpy(data_buf, &buffer[offset], actual_read_bytes);

					while (actual_read_bytes < packet.ContentLength)
					{
						numbytes = recv(ServerSocketFd, data_buf + actual_read_bytes, packet.ContentLength - actual_read_bytes, 0);
						if (numbytes > 0)
						{
							actual_read_bytes += numbytes;
						}
						else
						{
							std::printf("Error on read json data: %i", WSAGetLastError()); return;
						}
						std::cout << "Read bytes: " << numbytes << ", ";
						std::cout << "Left bytes: " << packet.ContentLength - actual_read_bytes << ", ";
						std::cout << "Total read bytes: " << actual_read_bytes << std::endl;
					}
					shutdown(ServerSocketFd, SD_SEND);
					ResourceInfoNode root;
					std::cout << "Parsing JSON data" << std::endl;
					ParseJSON(data_buf, root);
					std::unordered_set<std::string> urls;
					AddLinks(&root, 0, urls);
					std::cout << "----------------------------------------------" << std::endl;
					for (const auto& url : urls)
					{
						std::cout << url << std::endl;
					}
					std::cout << "----------------------------------------------" << std::endl;
				}
			}
			else std::cout << "Error in get info. Try connect and get command again" << std::endl;
		}
	}

	
	UserCommands CrawlerClient::GetUserCommand()
	{
		UserCommands command;
		std::string command_str;
		std::getline(std::cin, command_str);
		//std::cin >> command_str;
		if (command_str.substr(0, 7) == "connect")
			command = CONNECT;
		else if (command_str.substr(0, 3) == "get")
		{
			command = GET;
			//url for start
			std::string tmp;
			std::cout << "Enter url(default = www.pythonanywhere.com): ";
			std::getline(std::cin, tmp);
			if (tmp.empty()) Url = "https://www.pythonanywhere.com/";
			else Url = tmp;
			//Search string
			std::cout << "Enter search words (default = python): ";
			std::getline(std::cin, tmp);
			if (tmp.empty() || tmp.size() > 64)
				SearchStr = "python";
			else SearchStr = tmp;
			//Deep count
			std::cout << "Enter deep count (not more than 5 is allowed): ";
			std::getline(std::cin, tmp);
			if (tmp.size() == 1)
			{
				const int count = std::stoi(tmp);
				if (count > 5) DeepCount = 2;
				else DeepCount = count;
			}
			else DeepCount = 2;
		}
		else if (command_str == "exit")
			command = EXIT;
		else
			command = IDLE;
		return command;
	}

	void CrawlerClient::Run()
	{
		while (IsRunning)
		{
			const UserCommands command = GetUserCommand();
			switch(command)
			{
			    case UserCommands::IDLE   : std::cout << "Unknown command" << std::endl; break;
				case UserCommands::CONNECT: EstablishConnection();                       break;
				case UserCommands::GET    : SendGET(); RecieveData();                    break;
				case UserCommands::EXIT   : Shutdown();                                  break;
			}
		}
	}

	void CrawlerClient::AddLinks(ResourceInfoNode* node_ptr, int deep_coef, std::unordered_set<std::string>& urls)
	{
		if (node_ptr->Found)
			urls.emplace(node_ptr->Url);
		
		for (auto& it : node_ptr->Links)
		{
			AddLinks(&it, deep_coef + 1, urls);
		}
	}
	
	/*-----------------------------Parser Section------------------------------------*/
	size_t ParseAnswer(const char* buf, HTTPAnswerPacket& packet)
	{
		std::vector<std::string> tokens;
		size_t actual_read_bytes = 0;
		//split to lines
		{
			std::stringstream lineStream(buf);
			for (std::string each; std::getline(lineStream, each, '\n'); tokens.push_back(each))
			{
				if (!tokens.empty() && tokens.back()[0] == '\r')
				{
					break;
				}
				else actual_read_bytes += each.size() + 1; //size of str and return symbol
			}
		}

		//split lines to 
		for (size_t i = 0; i < tokens.size(); i++)
		{
			std::stringstream lineStream(tokens[i]);
			std::vector<std::string> inner_tokens;
			for (std::string each; std::getline(lineStream, each, ':'); inner_tokens.push_back(each));

			if (!STR_EQUAL(inner_tokens[0], "\r"))
			{
				if (STR_EQUAL(inner_tokens[0], "Content-Type")) inner_tokens[0] = "Type";
				else if (STR_EQUAL(inner_tokens[0], "Content-Length")) inner_tokens[0] = "Length";
				inner_tokens[1] = TRIM(inner_tokens[1]);
			}
			SWITCH(inner_tokens[0].c_str())
			{
				CASE("Type")   : packet.ContentType   = inner_tokens[1];            break;
				CASE("Length") : packet.ContentLength = std::strtoull(inner_tokens[1].c_str(), nullptr, 10); break;
				CASE("Server") : packet.Server        = inner_tokens[1];            break;
				CASE("Date")   : packet.Date          = inner_tokens[1];            break;
			}
		}
		return actual_read_bytes;
	}

	void ParseJSONObject(std::vector<std::string>& tokens, size_t& i, ResourceInfoNode& crawler_node)
	{
		//parsed info into object
		for (i; i < tokens.size(); i++)
		{
			std::stringstream lineStream(tokens[i]);
			std::string inner_key;
			std::getline(lineStream, inner_key, ':');
			const size_t offset = inner_key.size() + 1;
			std::string inner_value;
			if (tokens[i].size() > offset)
			{
				inner_value = tokens[i].substr(offset, tokens[i].size() - offset);
				STR_TRIM_COMMA(inner_value);
				STR_TRIM_SPACE(inner_value);
				STR_TRIM_QUOTE(inner_value);
			}
			else inner_value = "";
			STR_TRIM_KEY(inner_key);
			SWITCH(inner_key.c_str())
			{
				CASE("count") :
				{
					if (!inner_value.empty())
					{
						STR_TRIM_VALUE(inner_value);
						crawler_node.Count = std::stoi(inner_value);
					}
				} break;
				CASE("links") :
				{
					if (inner_value != "[]")
					{
						crawler_node.Links.resize(crawler_node.Count);
						ParseJSONObjArray(tokens, i, crawler_node.Links);
					}
					else crawler_node.Count = 0;
				} break;
				CASE("desc") :
				{
					crawler_node.Description = inner_value;
					
				} break;
				CASE("title") :
				{
					crawler_node.Title = inner_value;
				} break;
				CASE("url") :
				{
					crawler_node.Url = inner_value;
				} break;
				CASE("found") :
				{
					if (inner_value == "true")
					{
						crawler_node.Found = true;
					}
					else crawler_node.Found = false;
				} break;
				CASE("}") : i++; return;
			}
		}
	}

	void ParseJSONObjArray(std::vector<std::string>& tokens, size_t& i, std::vector<ResourceInfoNode>& crawler_array)
	{
		//[ { key: val, ...}, {key: val, ...}, ... ]
		for (size_t j = 0; j < crawler_array.size(); j++)
			ParseJSONObject(tokens, i, crawler_array[j]);
	}

	void ParseJSONArray(char** buf, size_t length, std::vector<int>& crawler_array)
	{
		std::vector<std::string> tokens;
		std::string str;
		str.resize(length);
		memcpy((void*)str.c_str(), *buf, length);
		std::stringstream lineStream(str);
		size_t actual_read_length = 0;
		for (std::string each; std::getline(lineStream, each, ','); tokens.emplace_back(each))
			actual_read_length += each.size();
		tokens.back() = tokens.back().substr(0, tokens.back().length() - 1);
		//[id1, id2, ..., idn]
		for (auto& token : tokens)
			crawler_array.push_back(std::stoi(token));
		//update buff adr
		*buf += actual_read_length;
	}

	void Tokenize(char* buf, std::vector<std::string>& tokens)
	{
		std::stringstream lineStream(buf);
		for (std::string each; std::getline(lineStream, each, '\n'); tokens.push_back(each));
	}

	void ParseJSON(char* buf, ResourceInfoNode& crawler_root_node)
	{
		std::vector<std::string> tokens;
		Tokenize(buf, tokens);
		size_t i = 1;
		ParseJSONObject(tokens, i, crawler_root_node);
	}



}

void InitializeWinSock2()
{
	//Initialize Winsock
	WSADATA WsaData;
	WORD DLLVersion = MAKEWORD(2, 2);
	//if Initializing is ok
	if (WSAStartup(DLLVersion, &WsaData) != 0)
	{
		std::printf("WinSock process initialized with error\n");
		exit(1);
	}
}

void main()
{
	InitializeWinSock2();
	Crawler101202::CrawlerClient client;
	client.Run();
	return;
}