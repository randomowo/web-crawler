# API:

#### Request
GET `/?url=\<url\>&depth=\<depth\>&search=<search>`

`<url> : str - root url`

`<depth> : int - max depth (не обязательно)`

`<search> : str - search word`

#### Response
```json
{
    "count": int,
    "found": bool,
    "desc": str,
    "links": [{
        "count": int,
        ...
        }, ... ],
    "title": string,
    "url": str
}
```
