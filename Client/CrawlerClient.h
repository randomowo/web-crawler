﻿#pragma once

#include <unordered_set>
#include <vector>
#include <iostream>
#include <string>
#include <cstdint> //fixed size types

//WinSock headers
#include <WinSock2.h>
#include <ws2ipdef.h>
#include <WS2tcpip.h>

#define bzero(buffer, len) (memset((buffer), '\0', (len)), (void) 0)

//Server Info
#define PortNumber "5000"
#define CRAWLER_IP_ADDR "127.0.0.1"

//buffer sizes
#define BUFFER_SIZE 128
#define BUFFER_GET_SIZE 256
#define BUFFER_RECIEVE_SIZE 2048

#define GET_REQ(arg) arg[0] = 'G'; arg[1] = 'E'; arg[2] = 'T';

//Status Codes
#define CHECK_STATUS_OK(code) (code[0] == '2' && code[1] == '0' && code[2] == '0')

namespace Crawler101202 {

	enum UserCommands
	{
		IDLE    = 0,
		CONNECT = 1,
		GET     = 2,
		EXIT    = 3
	};
	
	struct ResourceInfoNode
	{
		size_t Count = 0;
		std::string Description;
		std::string Title;
		std::string Url;
		std::vector<ResourceInfoNode> Links;
		bool Found = false;
	};

#pragma pack(push, 1)
	struct HTTPAnswerHeader
	{
		char ResponseVersion[8];
		char Space1;
		char StatusCode[3];
		char Space2;
		char ResponsePhrase[2];
		char End[2];
	};
#pragma pack(pop)

	struct HTTPAnswerPacket
	{
		std::string ContentType;
		size_t ContentLength = 0;
		std::string Server;
		std::string Date;
	};
	
	class CrawlerClient
	{
	public:
		CrawlerClient();
		~CrawlerClient();

		void Run();
	private:
		//User Interface
		UserCommands GetUserCommand();

		//TCP connect
		void EstablishConnection();

		//Client Http Functions
		void SendGET();
		void Deserealize();
		void DesearealizeAnswerPacket();
		void RecieveData();

		void AddLinks(ResourceInfoNode* node_ptr, int deep_coef, std::unordered_set<std::string>& urls);
		
		inline void Shutdown() { IsRunning = false; };
		
	private:
		SOCKET ServerSocketFd;
		bool IsRunning;
		std::string Url;
		std::string SearchStr;
		char DeepCount;
	};


	
	/*-----------------------------Parser Section------------------------------------*/
	size_t ParseAnswer(const char* buf, HTTPAnswerPacket& packet);

	//Parse tokens
	void ParseJSONObject(std::vector<std::string>& tokens, size_t& i, ResourceInfoNode& crawler_node);
	void ParseJSONObjArray(std::vector<std::string>& tokens, size_t& i, std::vector<ResourceInfoNode>& crawler_array);
	void ParseJSONArray(char** buf, size_t length, std::vector<int>& crawler_array);

	//main JSON parse func
	void Tokenize(char* buf, std::vector<std::string>& tokens);
	void ParseJSON(char* buf, ResourceInfoNode& crawler_root_node);

#define TRIM(str) str.substr(1, str.size() - 2);
#define STR_EQUAL(str1, str2) (str1.compare(str2) == 0)
#define STR_TRIM_KEY(str) str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());\
	str.erase(std::remove(str.begin(), str.end(), ' '), str.end());\
	str.erase(std::remove(str.begin(), str.end(), ','), str.end())
#define STR_TRIM_VALUE(str) str.erase(std::remove(str.begin(), str.end(), ','), str.end());\
	str.erase(std::remove(str.begin(), str.end(), ' '), str.end())
#define STR_TRIM_QUOTE(str) str.erase(std::remove(str.begin(), str.end(), '\"'), str.end())
#define STR_TRIM_SPACE(str) str.erase(std::remove(str.begin(), str.end(), ' '), str.end())
#define STR_TRIM_COMMA(str) str.erase(std::remove(str.begin(), str.end(), ','), str.end())
	
	typedef unsigned char u_char;
	typedef unsigned long long u_llong;
	const u_char MAX_LEN = 9;
	const u_llong N_HASH = static_cast<u_llong>(-1);
	
#define CASE(str)  static_assert(str_is_correct(str) && (str_len(str) <= MAX_LEN),\
   "CASE string contains wrong characters, or its length is greater than 9");\
    case str_hash(str, str_len(str))
	
	//if met null return 0
	//else increment str pointer
	constexpr unsigned char str_len(const char* const str)
	{
		return *str ? (1 + str_len(str + 1)) : 0;
	}

	//signed char contains unsigned 0 - 127 numbers
	constexpr bool str_is_correct(const char* const str)
	{
		return (static_cast<signed char>(*str) > 0) ? str_is_correct(str + 1) : (*str ? false : true);
	}

	constexpr u_llong raise_128_to(const u_char power)
	{
		return 1ULL << 7 * power;
	}

	//hash = str[i] * 128 ^ (len - i)
	constexpr u_llong str_hash(const char* const str, const u_char current_len)
	{
		return *str ? (raise_128_to(current_len - 1) * static_cast<u_char>(*str)
			+ str_hash(str + 1, current_len - 1)) : 0;
	}

#define SWITCH(str)  switch(str_hash_for_switch(str))
	
	inline u_llong str_hash_for_switch(const char* const str)
	{
		return (str_is_correct(str) && (str_len(str) <= MAX_LEN)) ? str_hash(str, str_len(str)) : N_HASH;
	}
}